package edu.week9.hibernate;

import java.util.*;

public class gamesPlayed {

    public static void main(String[] args) {

        Connection gamesList = Connection.getInstance();

        System.out.println("This program connects to a database which houses a list of video games that you have beaten " +
                "in 2020. It allows you to add more games then prints the list of games in the database.");
        System.out.println();
        System.out.println("Have you beaten another game that you would like to add 'Y' or 'N'?");

        Scanner addGameDetails = new Scanner(System.in);
        String addGame = addGameDetails.next();
        System.out.println();
        if (addGame.toUpperCase().charAt(0) == 'Y') {
            gamesList.addGame();
        }

        // print the list of beaten games from the database.
        List<Game> game = gamesList.getGames();
        for (Game gameList : game) {
            System.out.println(gameList);
        }

        // goodbye message
        System.out.println("Thank you for using this service. Hopefully you are having fun with all those games!");
    }
}
