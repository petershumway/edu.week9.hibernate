
// Part of this class is the code Brother Tuckett provided in the instructional material. I started with that tutorial
// code to make sure my database was working and I could establish the connection. I am still using the portion for the
// Singleton approach as well as the getInstance() method. the getGames method is heavily based on his getCustomer method.
// Because it doesnt make sense to me to rewrite the code he already provided in the same format to accomplish the same
// thing, I instead have done my best to add additional notes to show my understanding of what I have learned as I
// studied the code.

package edu.week9.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import java.util.*;

public class Connection {

    // Data member variables for the session, session factory & unique instance of the class. This
    // makes sure that there is only one instance of the class and that it is thread safe through the
    // session factory (in the HibernateUtils.java class).
    SessionFactory factory = null;
    Session session = null;

    private static Connection single_instance = null;

    // gets the session factory when the instance is created.
    private Connection()
    {
        factory = HibernateUtils.getSessionFactory();
    }

    // checks to make sure there is no instance yet and creates a new one. if there is an instance then it returns the
    // existing instance.
    public static Connection getInstance()
    {
        if (single_instance == null) {
            single_instance = new Connection();
        }

        return single_instance;
    }

    // this method opens a session and connects to the database. it sends a SQL query to get the data from the database.
    // it then stores the data in the LIST container, saves the interaction, and returns the LIST.
    public List<Game> getGames() {

        // try to make sure it works correctly for error checking
        try {
            // open a new session
            session = factory.openSession();
            session.getTransaction().begin();
            // submit SQL query
            String sql = "from edu.week9.hibernate.Game";
            List<Game> games = (List<Game>)session.createQuery(sql).getResultList();
            // commit changes
            session.getTransaction().commit();
            // return the list
            return games;
        }
        //Catch any exceptions, show an error message, rollback any changes, return nothing.
        catch (Exception e) {
            System.out.println("There was an error retrieving the data from the database.");
            // Rollback any changes so they are not saved.
            session.getTransaction().rollback();
            return null;
        }
        // always close the session to avoid any issues.
        finally {
            session.close();
        }
    }

    // this is a method I added to add a new game line to the database.
    public void addGame() {
        String addGame = "Y";
        Scanner addStudentInput = new Scanner(System.in);

        // if user enters Y then prompt for student informaton
        while (addGame.toUpperCase().charAt(0) == 'Y') {
            // Set so that the question will ask again.
            addGame = "N";

            System.out.println("Please enter the following details for the newly beaten game:");

            //get name
            String gameName = getStringInput("Game Name");
            //get dev
            String gameDev = getStringInput("Game Developer");
            //get pub
            String gamePub = getStringInput("Game Publisher");
            //get platform
            String platform = getStringInput("Game Platform");

            // try to make sure it works correctly for error checking
            try {
                // open a new session
                session = factory.openSession();
                session.getTransaction().begin();

                // create the new game object
                Game newGame = new Game();
                newGame.setGameName(gameName);
                newGame.setGameDev(gameDev);
                newGame.setGamePub(gamePub);
                newGame.setPlatform(platform);

                // save the new object
                session.save(newGame);
                // commit the change to the database
                session.getTransaction().commit();
            }
            catch (Exception e) {
                System.out.println("There was an error adding the game information to the database.");
                // Rollback any changes so they are not saved.
                session.getTransaction().rollback();
            }
            // always close the session
            finally {
                session.close();
            }

            // provide option to continue adding students
            System.out.println();
            System.out.print("Type 'Y' to add another game or 'N' to stop adding games: ");
            addGame = addStudentInput.nextLine();
            System.out.println();

        }
    }

    // simple class I wrote to get string input with error checking included.
    String getStringInput(String dataType) {
        // Create Scanner object for input
        Scanner input = new Scanner(System.in);

        boolean continueInput;
        String newInput = "error";

        do {
            // error handling for mismatched input
            try {
                System.out.println("Please enter " + dataType + ": ");

                newInput = input.nextLine();

                // as long as there was no exception continue to the next input
                continueInput = false;
            }
            // catch any input mismatch errors
            catch (InputMismatchException ex) {
                // display the error message
                System.out.println("Error: The input must be text characters or numbers.\n");
                // clear input
                input.nextLine();
                // make sure the loop will happen again for the current string
                continueInput = true;
            }
        } while (continueInput);

        return newInput;
    }
}
