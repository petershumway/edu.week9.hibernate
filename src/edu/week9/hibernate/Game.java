package edu.week9.hibernate;

import javax.persistence.*;

// this connects the class to the entity in the database
@Entity
@Table(name = "games2020")

// This class houses the game object which matches the table in the database. It holds the details of games for this
// program.
public class Game {

    // the game num class is automatically incremented by one when a new game is added.
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int gameNum;

    @Column(name = "gameName", unique = true, nullable = false, length = 50)
    private String gameName;

    @Column(name = "gameDev", nullable = false, length = 50)
    private String gameDev;

    @Column(name = "gamePub", nullable = false, length = 50)
    private String gamePub;

    @Column(name = "platform", nullable = false, length = 50)
    private String platform;

    public int getGameNum() {
        return gameNum;
    }

    public void setGameNum(int gameNum) {
        this.gameNum = gameNum;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public String getGameDev() {
        return gameDev;
    }

    public void setGameDev(String gameDev) {
        this.gameDev = gameDev;
    }

    public String getGamePub() {
        return gamePub;
    }

    public void setGamePub(String gamePub) {
        this.gamePub = gamePub;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String toString() {
        return "Game: " + Integer.toString(this.gameNum) + " \nTitle: " + this.gameName + " \nDeveloper: "
                + this.gameDev + " \nPublisher: " + this.gamePub + " \nPlatform: " + this.platform + "\n";
    }
}
